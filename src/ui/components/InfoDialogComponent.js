import SweetAlert from 'sweetalert2-react';
import PropTypes from 'prop-types';

export default function InfoDialogComponent(props) {

    return <SweetAlert
        show={props.show}
        title="Info!"
        text={props.message}
        type={"info"}
        onConfirm={props.onConfirm}
    />;
}

InfoDialogComponent.propTypes = {
    show: PropTypes.bool.isRequired,
    message: PropTypes.string.isRequired,
    onConfirm: PropTypes.func.isRequired
}

InfoDialogComponent.defaultProps = {
    message: ""
}
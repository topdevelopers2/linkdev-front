import User from "../model/User";

export default class AppCache {

    /**
     * @type {AppCache}
     */
    static appCache;
    /**
     * @private
     * @type {User}
     */
    user;

    static getInstance() {
        if (AppCache.appCache == null) {
            AppCache.appCache = new AppCache();
        }
        return AppCache.appCache;
    }

    /**
     *  save user in cache.
     * @param {User} user
     */
    saveUser(user) {
        this.user = user;
        localStorage.setItem("user", JSON.stringify(user.toJson()));
    }

    getUser() {
        if (this.user) {
            return this.user;
        }
        // check if user exists in cache , if exists parse it else return null.
        const userInCache = localStorage.getItem("user");
        if (userInCache) {
            this.user = new User(JSON.parse(userInCache));
            return this.user;
        }
        return null;
    }

    hasUser() {
        return this.getUser() != null;
    }

    clearCache() {
        this.user = null;
        localStorage.clear();
    }
}